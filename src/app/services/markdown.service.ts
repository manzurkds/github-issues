import { Injectable } from '@angular/core';

declare var showdown;

@Injectable()
export class MarkdownService {

  converter: any;

  constructor() {
    this.converter = new showdown.Converter()
   }

  convertToMarkdown(text) {
    return this.converter.makeHtml(text)
  }

}
