import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GithubService {

  apiBase: string = 'https://api.github.com';
  owner = 'angular';
  repo = 'angular';

  constructor(
    private http: HttpClient,
  ) {}


  getIssues(owner, repo, page=1) {
    return this.http.get(`${this.apiBase}/repos/${owner}/${repo}/issues?page=${page}`)
  }

  getIssue(owner, repo, issueNumber) {
    return this.http.get(`${this.apiBase}/repos/${owner}/${repo}/issues/${issueNumber}`)
  }

  getComments(commentsUrl) {
    return this.http.get(commentsUrl)
  }

}
