import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'getAge'
})
export class GetAgePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    
    if(!value || value === NaN) return null
    
    var date = new Date(value);

    var text;

    var now = moment(new Date())
    var comparisionDate = moment(date)

    var diff = now.diff(comparisionDate, 'days')

    if(diff === 0) {
      text = 'today'
      return text
    }
    
    if(diff === 1) text = '1 day ago'
    else text = `${diff} days ago`

    if(diff > 30) {
      diff = now.diff(comparisionDate, 'months')
      if(diff === 1) text = 'a month ago'
      else text = `${diff} months ago`
      
      if(diff > 12) {
        diff = now.diff(comparisionDate, 'years')
        if(diff === 1) text = 'a year ago'
        else text = `${diff} years ago`
      }
    }

    return text;
          
  }

}