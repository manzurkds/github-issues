import { Component, OnInit, Input, Output } from '@angular/core';
import { GithubService } from '../../../services/github.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  
  owner: string;
  repo: string;
  issues: any;
  page = 1;
  issuesLoading: any;

  constructor(
    private github: GithubService
  ) { 
    this.owner = this.github.owner;
    this.repo = this.github.repo;
  }

  ngOnInit() {
    this.issuesLoading = true
    this.github.getIssues(this.owner, this.repo)
    .subscribe((issues: Array<any>) => {
      this.issuesLoading = false
      this.issues = issues
    }, err => {
      this.issuesLoading = false
      console.error('err fetching issues: ', err)
    })
  }

  getIssuesForPage(page) {
    this.page = page;

    this.github.getIssues(this.owner, this.repo, page)
    .subscribe((issues: Array<any>) => {
      this.issues = issues
      this.goTop()
    }, err => {
      console.error('err fetching issues: ', err)
    })
  }

  goTop() {
    window.scrollTo(0, 0)
  }

}
