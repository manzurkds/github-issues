import { Component, OnInit, Inject } from '@angular/core';
import { GithubService } from '../../services/github.service';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnInit {

  owner: string = 'angular';
  repo: string = 'angular';

  constructor(
    private github: GithubService
  ) { }

  ngOnInit() {
  }
}
