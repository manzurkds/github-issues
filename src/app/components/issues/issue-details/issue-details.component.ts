import { Component, OnInit, Input } from '@angular/core';
import { GithubService } from '../../../services/github.service';
import { MarkdownService } from '../../../services/markdown.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-issue-details',
  templateUrl: './issue-details.component.html',
  styleUrls: ['./issue-details.component.scss']
})
export class IssueDetailsComponent implements OnInit {

  owner = this.github.owner;
  repo = this.github.repo;
  issue: any;
  markdownBody: any;
  comments: any;
  issueLoading: any;
  commentsLoading: any;

  constructor(
    private github: GithubService,
    private markdown: MarkdownService,
    private _activatedRoute: ActivatedRoute
  ) {
    this._activatedRoute.params.subscribe(params => {
      this.getIssue(params.number)
    })
   }

  ngOnInit() {
  }

  getIssue(number) {
    this.issueLoading = true

    this.github.getIssue(this.owner, this.repo, number)
    .subscribe((issue: any) => {
      this.issueLoading = false
      this.issue = issue
      this.markdownBody = this.markdown.convertToMarkdown(issue.body)
      if(issue.comments) {
        this.commentsLoading = true
        this.github.getComments(issue.comments_url)
        .subscribe(comments => {
          this.commentsLoading = false
          this.comments = comments
        }, err => {
          this.commentsLoading = true
        })
      }
    }, err => {
      this.issueLoading = false
    })
  }

}
