import { Component, OnInit, Input, Output } from '@angular/core';
import { MarkdownService } from '../../../../services/markdown.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() comments: any;
  markdownBody: any;

  constructor(
    private markdownService: MarkdownService,
  ) { }

  ngOnInit() {
    if(this.comments && this.comments.length) {
      this.comments.forEach(comment => {
        comment.body = this.markdownService.convertToMarkdown(comment.body)
      })
    }
  }

}
