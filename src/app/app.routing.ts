import { RouterModule, Routes } from '@angular/router';
import { IssuesComponent } from './components/issues/issues.component';
import { ListComponent } from './components/issues/list/list.component';
import { IssueDetailsComponent } from './components/issues/issue-details/issue-details.component';

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'issues/list', pathMatch: 'full' },
    { path: 'issues', component: IssuesComponent, children: [
        { path: 'list', component: ListComponent },
        { path: 'details/:number', component: IssueDetailsComponent }
    ]}, 
]

export const AppRouting = RouterModule.forRoot(APP_ROUTES)