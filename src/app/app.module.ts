import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRouting } from './app.routing';

//Components
import { AppComponent } from './app.component';
import { IssuesComponent } from './components/issues/issues.component';
import { IssueDetailsComponent } from './components/issues/issue-details/issue-details.component';

//Services
import { GithubService } from './services/github.service';
import { MarkdownService } from './services/markdown.service';

//Pipes
import { GetAgePipe } from './pipes/get-age.pipe';
import { ListComponent } from './components/issues/list/list.component';
import { CommentsComponent } from './components/issues/issue-details/comments/comments.component';


@NgModule({
  declarations: [
    AppComponent,
    IssuesComponent,
    GetAgePipe,
    IssueDetailsComponent,
    ListComponent,
    CommentsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppRouting
  ],
  providers: [
    GithubService,
    MarkdownService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
